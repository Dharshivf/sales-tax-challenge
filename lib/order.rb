require 'product'
require 'product_classification'

class Order
  attr_reader :items

  def initialize(raw_item_list)
    @product_keys = %w[quantity name price]
    @raw_item_list = raw_item_list
    @items = []
  end

  def parse_item_list
    @raw_item_list.each do |raw_item_list|
      product_attr = @product_keys.zip(raw_item_list).to_h
      @items << Product.new(* product_attr.values)
    end
  end

  def calsify_item_list
    @items.each do |product|
      ProductClassification.new(product).classify
    end
  end
end
