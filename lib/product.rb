class Product
  attr_reader :quantity, :name, :price, :bsales_tax,
              :isales_tax, :total_sales_tax
  attr_accessor :bsales_tax_apply, :isales_tax_apply

  BASE_TAX = 10
  SALES_TAX = 5

  def initialize(quantity, name, price)
    @quantity = quantity.to_i
    @name = name.to_s.strip
    @price = price.to_f
    @bsales_tax = 0.0
    @isales_tax = 0.0
    @bsales_tax_apply = false
    @isales_tax_apply = false
    @total_sales_tax = 0.0
  end

  def to_h
    hash = {}
    instance_variables.each { |var| hash[var.to_s.delete('@')] = instance_variable_get(var) }
    hash
  end

  # tax rate of n%, a shelf price of p contains
  # (np/100 rounded up to the nearest 0.05) amount of sales tax.
  def sales_tax(tax, tax_type)
    tax_amount = @price * tax / 100
    tax_amount_rounded = (tax_amount * 20).ceil / 20.0

    tax_type == 'base' ? @bsales_tax = tax_amount_rounded : @isales_tax = tax_amount_rounded
  end

  def calculate_taxes
    sales_tax(BASE_TAX, 'base') if @bsales_tax_apply
    sales_tax(SALES_TAX, 'sales') if @isales_tax_apply
    @total_sales_tax = @bsales_tax + @isales_tax
  end

  def price_with_tax
    (price + bsales_tax + isales_tax).round(2)
  end
end
