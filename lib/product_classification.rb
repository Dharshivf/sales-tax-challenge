require 'input'

class ProductClassification
  def initialize(product)
    @product = product
  end

  def classify
    # Fix this
    excluded_items_list = Input.new('exclutions.txt').excluded_items

    product_name_str = @product.name.downcase.scan(/[\w']+/)
    excluded = product_name_str & excluded_items_list

    @product.bsales_tax_apply = !excluded.empty? ? false : true
    @product.isales_tax_apply = true if @product.name =~ /imported/i
  end
end
