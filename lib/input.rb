class Input
  attr_reader :purchase_items

  def initialize(file_name)
    @purchase_items = parse_items(file_name)
  end

  def parse_items(file_name)
    raw_item_list = []
     File.open("#{File.dirname(File.dirname(__FILE__))}/test-data/#{file_name}").each do |line|
       line.gsub!("\n", '')
       raw_item_list << line.split(',')
     end
     raw_item_list
  end

  def excluded_items
    items_excluded = parse_items('exclutions.txt')
    items_excluded.flatten!
    items_excluded.map! { |item| item.chomp.downcase }
  end
end
