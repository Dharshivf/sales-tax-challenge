class Receipt
  def initialize(order)
    @order = order
  end

  def print
    amount_total = 0.0
    sales_total = 0.0
    receipt_content, sales_total, amount_total = order_totals(amount_total, sales_total)
    receipt_content += "Sales Taxes: #{format('%.2f', sales_total)} \n"

    receipt_content + "Total: #{format('%.2f', amount_total)}"
  end

  def order_totals(amount_total, sales_total)
    receipt_content = ''
    @order.items.each do |line_item|
      if line_item.quantity.positive?
        line_item.calculate_taxes
        price_amount = (line_item.price_with_tax * line_item.quantity).to_f
        amount_total += price_amount

        sales_total += line_item.total_sales_tax
      end
      receipt_content += "#{line_item.quantity}, #{line_item.name}, #{format('%.2f', price_amount)} \n"
    end
    [receipt_content, sales_total, amount_total]
  end
end
