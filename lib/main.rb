$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'input'
require 'order'
require 'receipt'

Dir.chdir('test-data') do
  test_data = Dir.glob('**/input*.txt')
  test_data.each do |test_input_file|
    input = Input.new(test_input_file)
    raw_item_list = input.parse_items(test_input_file)

    puts "-------------------------------------------"
    puts "Input\n"
    raw_item_list.each do |item|
      puts item.join(',')
    end

    order = Order.new(raw_item_list)
    order.parse_item_list
    order.calsify_item_list
    receipt = Receipt.new(order)
    puts "Receipt\n"
    puts receipt.print
    puts "-------------------------------------------"
  rescue Errno::ENOENT => e
    puts "Following error occured while reading file #{e}"
    exit
  end
end
