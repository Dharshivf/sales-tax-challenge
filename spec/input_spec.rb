require 'spec_helper'
require 'input'

describe Input do
  let(:input_file) { 'input-1.txt' }
  let(:input) { Input.new(input_file) }

  describe '#parse_items' do
    it 'returns an array of items' do
      expect(input.parse_items(input_file)).to be_a Array
    end

    it 'retuns an array of excluded items' do
      expect(input.excluded_items).to be_a Array
    end
  end
end
