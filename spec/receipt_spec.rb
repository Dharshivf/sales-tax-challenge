require 'spec_helper'

require 'order'
require 'receipt'

describe Receipt do
  let(:raw_item_list) { [[1, 'book', 12.49], [1, 'music cd', 14.99], [1, 'chocolate bar', 0.85]] }
  let(:order) { Order.new(raw_item_list) }

  before(:each) do
    order.parse_item_list
    order.calsify_item_list
  end

  it 'prints order items' do
    receipt = Receipt.new(order)
    expect(receipt.print).to include('1, book, 12.49')
  end

  it 'prints sales taxes' do
    receipt = Receipt.new(order)
    expect(receipt.print).to include('Sales Taxes: 1.50')
  end

  it 'prints total' do
    receipt = Receipt.new(order)
    expect(receipt.print).to include('Total: 29.83')
  end
end
