require 'spec_helper'

require 'order'
require 'product'

describe Order do
  let(:raw_item_list) { [[1, 'book', 12.49], [1, 'music cd', 14.99]] }

  let(:order) { Order.new(raw_item_list) }

  describe '#order_items' do
    it 'returns list of products in order' do
      order.parse_item_list

      expect(order.items.first.to_h).to eq(Product.new(1, 'book', 12.49).to_h)
    end
  end
end
