require 'spec_helper'

require 'product'

describe Product do
  let(:product1) { Product.new(1, 'book', 12.49) }
  let(:product2) { Product.new(1, 'music cd', 14.99) }
  let(:product3) { Product.new(1, 'imported box of chocolates', 10.00) }
  let(:product4) { Product.new(1, 'imported bottle of perfume', 47.50) }
  let(:base_tax) { 10 }
  let(:sales_tax) { 5 }

  describe '#taxe calculations' do
    it 'returns no basic tax for excempted products' do
      shelf_price_with_base_tax = product1.price + product1.bsales_tax
      expect(shelf_price_with_base_tax).to eq(12.49)
    end

    it 'returns no basic tax for products that are not excempted' do
      product2.sales_tax(base_tax, 'base')
      shelf_price_with_base_tax = (product2.price + product2.bsales_tax).round(2)
      expect(shelf_price_with_base_tax).to eq(16.49)
    end

    it 'returns sales taxes for imported items for excempted items' do
      product3.sales_tax(sales_tax, 'sales')
      shelf_price_with_import_tax = (product3.price + product3.isales_tax).round(2)
      expect(shelf_price_with_import_tax).to eq(10.50)
    end

    it 'returns sales taxes for imported items for non excempted items' do
      product4.sales_tax(base_tax, 'base')
      product4.sales_tax(sales_tax, 'sales')
      shelf_price_with_both_taxex = (product4.price + product4.isales_tax + product4.bsales_tax).round(2)
      expect(shelf_price_with_both_taxex).to eq(54.65)
    end
  end
end
